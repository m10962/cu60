#define First // LAST First
#if First
#region snippet_first
using ContosoUniversity.Models;
using Microsoft.AspNetCore.Server.Kestrel.Core.Features;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using NuGet.Protocol;

namespace ContosoUniversity.Data
{
    public class SchoolContext : DbContext
    {
        protected readonly IConfiguration Configuration;

        private SqlConnectionStringBuilder _connectionString;
        public SchoolContext(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public void SetConnectionString(string connectionString)
        {

            // Get the connectionStrings section.
            Configuration["SchoolContext"] = connectionString;
            _connectionString = new SqlConnectionStringBuilder(connectionString);

            try
            {
                SqlDbInitializer init = new SqlDbInitializer();
                init.InitDatabase(_connectionString);
                DbInitializer.Initialize(this);
            }
            catch { }

        }
        public SqlConnectionStringBuilder ConnectionString { get
            {
                if (_connectionString == null)
                {
                    InitDbString();
                }
                if (_connectionString == null)
                {
                    _connectionString = new SqlConnectionStringBuilder();
                    return _connectionString;
                }
                return _connectionString;
            }
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            InitDbString();
            try
            {
                if (!string.IsNullOrEmpty(_connectionString.ConnectionString))
                {
                    // connect to sql server with connection string from app settings
                    options.UseSqlServer(_connectionString.ConnectionString);
                }
            }
            catch
            { }
        }
        internal void InitDbString()
        {
            try
            {
                _connectionString = new SqlConnectionStringBuilder(Environment.GetEnvironmentVariable("SchoolContext"));
            }
            catch
            {
            }
            try
            {
                if (_connectionString == null || string.IsNullOrEmpty(_connectionString.ConnectionString))
                {
                    if (!string.IsNullOrEmpty(Configuration["SchoolContext"]))
                    {
                        _connectionString = new SqlConnectionStringBuilder(Configuration["SchoolContext"]);
                    }
                }
            }
            catch
            {
            }
            try
            {
                if (_connectionString == null || string.IsNullOrEmpty(_connectionString.ConnectionString))
                {
                    _connectionString = new SqlConnectionStringBuilder(Configuration.GetConnectionString("SchoolContext"));
                }
            }
            catch
            {
            }
        }


        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Course> Courses { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>().ToTable("Course");
            modelBuilder.Entity<Enrollment>().ToTable("Enrollment");
            modelBuilder.Entity<Student>().ToTable("Student");
        }
    }
}
#endregion
#elif LAST
#endif
