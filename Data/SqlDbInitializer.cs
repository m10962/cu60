using System.Data;
using System.Security.AccessControl;
using System.Security.Principal;
using Microsoft.Data.SqlClient;
using static System.Net.Mime.MediaTypeNames;

namespace ContosoUniversity.Data
{
    public class SqlDbInitializer
    {
        public bool InitDatabase(SqlConnectionStringBuilder sqlConnectionStringBuilder)
        {
            SqlConnectionStringBuilder newStringBuilder = new SqlConnectionStringBuilder(sqlConnectionStringBuilder.ConnectionString);
            newStringBuilder.InitialCatalog = "master";

            SqlConnection mycon = new SqlConnection(newStringBuilder.ConnectionString);

            try
            {
                mycon.Open();
                if (CheckDatabaseExists(mycon, sqlConnectionStringBuilder.InitialCatalog))
                {
                    return true;
                }
                CreateDatabase(mycon, sqlConnectionStringBuilder.InitialCatalog);
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                if (mycon.State == ConnectionState.Open)
                {
                    mycon.Close();
                }
            }
            return true;

        }
        private bool CreateDatabase(SqlConnection connection, string txtDatabase)
        {
            String CreateDatabase;
            string appPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            bool IsExits = CheckDatabaseExists(connection, txtDatabase); //Check database exists in sql server.
            if (!IsExits)
            {
                CreateDatabase = "CREATE DATABASE " + txtDatabase + " ; ";
                SqlCommand command = new SqlCommand(CreateDatabase, connection);
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (System.Exception)
                {
                    return false;
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
                return true;
            }

            return false;
        }

        private bool CheckDatabaseExists(SqlConnection tmpConn, string databaseName)
        {
            string sqlCreateDBQuery;
            bool result = false;

            try
            {
                sqlCreateDBQuery = string.Format("SELECT database_id FROM sys.databases WHERE Name = '{0}'", databaseName);
                using (SqlCommand sqlCmd = new SqlCommand(sqlCreateDBQuery, tmpConn))
                {
                    tmpConn.Open();
                    object resultObj = sqlCmd.ExecuteScalar();
                    int databaseID = 0;
                    if (resultObj != null)
                    {
                        int.TryParse(resultObj.ToString(), out databaseID);
                    }
                    tmpConn.Close();
                    result = (databaseID > 0);
                }
            }
            catch (Exception)
            {
                result = false;
            }
            return result;
        }
    }
}
