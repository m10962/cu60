﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ContosoUniversity.Data;
using ContosoUniversity.Models;
using System.Data.Common;
using Microsoft.Data.SqlClient;

namespace ContosoUniversity.Pages.About
{
    public class EditModel : PageModel
    {
        private ContosoUniversity.Data.SchoolContext _context;
        private SqlConnectionStringBuilder _connectionString;
        public EditModel(ContosoUniversity.Data.SchoolContext context)
        {
            _context = context;
            _connectionString = _context.ConnectionString;
        }

        [BindProperty]
        public string Server { get; set; }

        [BindProperty]
        public string Database { get; set; }

        [BindProperty]
        public string UserId { get; set; }

        [BindProperty]
        public string Password { get; set; }

        public IActionResult OnGet()
        {
            try
            {
                Server = ExtractString(_connectionString, "Server");
                Database = ExtractString(_connectionString, "Database");
                UserId = ExtractString(_connectionString, "User Id");
                Password = "Password";
            }
            catch
            {
                return RedirectToPage("./Index");
            }
            return Page();
        }

        private static string ExtractString(DbConnectionStringBuilder dbString, string parameter)
        {
            if (dbString != null)
            {
                if (dbString.ContainsKey(parameter))
                {
                    object value;
                    if (dbString.TryGetValue(parameter, out value))
                    {
                        return value.ToString();
                    }
                }
            }
            return "";

        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public IActionResult OnPost()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            try
            {
                _connectionString.DataSource = Server;
                _connectionString.InitialCatalog = Database;
                _connectionString.UserID = UserId;
                _connectionString.Password = Password;
                _context.SetConnectionString(_connectionString.ConnectionString);
            }
            catch (DbUpdateConcurrencyException)
            {
            }

            return RedirectToPage("./Index");
        }

        private bool StudentExists(int id)
        {
            return _context.Students.Any(e => e.ID == id);
        }
    }
}
