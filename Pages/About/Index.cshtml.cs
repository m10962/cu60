using ContosoUniversity.Data;
using ContosoUniversity.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace ContosoUniversity.Pages.About
{
    public class IndexModel : PageModel
    {
        private readonly ContosoUniversity.Data.SchoolContext _context;

        public IndexModel(ContosoUniversity.Data.SchoolContext context)
        {
            _context = context;
        }

        public bool ConnectionSuccess { get; set; }

        public string ConnectionString { get; set; }

        public async Task OnGetAsync()
        {
            if (_context.ConnectionString == null)
            {
                ConnectionSuccess = false;
                ConnectionString = "";
            }
            else
            {
                ConnectionSuccess = await _context.Database.CanConnectAsync();
                ConnectionString = _context.ConnectionString.ConnectionString;
                DbInitializer.Initialize(_context);
            }
        }
    }
}
