﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ContosoUniversity.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ContosoUniversity.Data.SchoolContext _context;

        public IndexModel(ContosoUniversity.Data.SchoolContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            Task<bool> t = _context.Database.CanConnectAsync();
            t.Wait();
            if (t.Result)
            {
                return Page();
            }
            return RedirectToPage("/About/Edit");
        }
    }
}