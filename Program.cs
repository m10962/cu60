using Microsoft.EntityFrameworkCore;
using ContosoUniversity.Data;
using NLog;
using NLog.Config;
using NLog.Targets;
using NLog.AWS.Logger;
using LogLevel = NLog.LogLevel;

string connectionString = "";
try
{
    connectionString = Environment.GetEnvironmentVariable("SchoolContext");
}
catch
{ }
var builder = WebApplication.CreateBuilder(args);
if (string.IsNullOrEmpty(connectionString))
{
    connectionString = builder.Configuration.GetConnectionString("SchoolContext");
}
// Setup the NLog configuration
var config = new LoggingConfiguration();
config.AddRule(LogLevel.Trace, LogLevel.Fatal, new ConsoleTarget());

// Add the AWS Target with minimal configuration
config.AddRule(LogLevel.Trace, LogLevel.Fatal, new AWSTarget()
{
    LogGroup = "/dotnet/contoso-app/nlog"
});

LogManager.Configuration = config;

// Create a new logger and test it
var log = LogManager.GetCurrentClassLogger();

log.Trace("Hello from BBB Module 109");

log.Trace("You have successuflly found my Message.");

log.Trace("Congratulations!");

builder.Services.AddDbContext<SchoolContext>(options =>
  options.UseSqlServer(connectionString));
builder.Services.AddDatabaseDeveloperPageExceptionFilter();

builder.Services.AddRazorPages();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;

    var context = services.GetRequiredService<SchoolContext>();

    try
    {
        DbInitializer.Initialize(context);
    }
    catch
    {
    }
}

//app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.MapRazorPages();

app.UseAuthorization();

app.MapRazorPages();

app.Run();
